package com.lien.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Bundle bd = getIntent().getExtras();
        String result= bd.getString("value");
        TextView rv = findViewById(R.id.rv);
        rv.setText(result);
    }
}